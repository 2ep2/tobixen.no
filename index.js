(async function () {
    let json = await getFeeSchedule()

    // loads latest fee schedule
    async function getFeeSchedule() {
        const url = "http://tobixen.no/fee.json"
        return await fetch(url).then(res => res.json())
    }

    let sellingCrypto = ['ETH', 'BCH', 'XBT', 'XBT-localbitcoins', 'XMR']
    let acceptedFiat = ['NOK-bank', 'NOK-cash', 'EUR-cash', 'XAU-canadian']
    let buyingCrypto = ['ETH', 'BCH', 'XBT', 'XBT-localbitcoins']
    let usingFiat = ['NOK-bank', 'NOK-cash', 'XAU-canadian']
    printCommissions(json)

    // optimally, this json would be cached for some time and shared with getRate
    function printCommissions(json) {
        // print last-updated
        let lastUpdated = document.getElementById('last-updated')
        lastUpdated.innerHTML = `last updated: ${json.updated}`

        // print commissions
        let commissions = json.commissions

        // selling Crypto
        let sellingDiv = document.getElementById('selling')
        sellingCrypto.forEach(crypto => {
            // get rates
            let cryptoOutRatio = commissions[crypto].out.ratio
            let cryptoOutFixed = commissions[crypto].out.fixed

            // format crypto label
            if (crypto.includes('-')) crypto = crypto.replace(/-/, ' (') + ')'

            acceptedFiat.forEach(fiat => {
                // get rates
                let fiatInRatio = commissions[fiat].in.ratio
                let fiatInFixed = commissions[fiat].in.fixed
                // format label
                let fiatLabel = fiat.replace(/-/, ' (') + ')'

                // calculate ratio and format label
                let exchange = `${(cryptoOutRatio + fiatInRatio) * 100}%`

                // add fixed fee if applicable
                let fixed = cryptoOutFixed + fiatInFixed
                if (fixed) exchange += ` + ${fixed} ${crypto}`

                // create div and populate with info
                let commDiv = document.createElement('div')
                commDiv.classList.add('striped--moon-gray', 'pl1')
                commDiv.innerHTML = `${crypto} for ${fiatLabel}, ${exchange}`

                // add div to the element in the DOM
                sellingDiv.appendChild(commDiv)
            })
        })

        // buying Crypto
        let buyingDiv = document.getElementById('buying')
        buyingCrypto.forEach(crypto => {
            // get rates
            let cryptoInRatio = commissions[crypto].in.ratio
            let cryptoInFixed = commissions[crypto].in.fixed
            // format label
            if (crypto.includes('-')) crypto = crypto.replace(/-/, ' (') + ')'

            usingFiat.forEach(fiat => {
                // get rates
                let fiatOutRatio = commissions[fiat].out.ratio
                let fiatOutFixed = commissions[fiat].out.fixed
                // format label
                let fiatLabel = fiat.replace(/-/, ' (') + ')'

                // calculate ratio and format label
                let exchange = `${(cryptoInRatio + fiatOutRatio) * 100}%`

                // add fixed fee if applicable
                let fixed = cryptoInFixed + fiatOutFixed
                if (fixed) exchange += ` + ${fixed} ${crypto}`

                // create div and populate with info
                let commDiv = document.createElement('div')
                commDiv.innerHTML = `${crypto} for ${fiatLabel}, ${exchange}`
                commDiv.classList.add('striped--moon-gray')

                // add div to the element in the DOM
                buyingDiv.appendChild(commDiv)
            })
        })
    }

    // turn on calculator
    let calculateButton = document.getElementById('calculate-button')
    calculateButton.addEventListener('click', calculate)

    let inputAmount = document.getElementById('amount')
    inputAmount.addEventListener('change', calculate)

    let reverseButton = document.getElementById('reverse-button')
    reverseButton.addEventListener('click', reverse)

    function reverse() {
        let crypto = document.getElementById('crypto').parentElement
        let fiat = document.getElementById('fiat').parentElement
        let _cryptoHTML = crypto.innerHTML
        crypto.innerHTML = fiat.innerHTML
        fiat.innerHTML = _cryptoHTML
        const tradeDir = document.getElementById('tradeDir')
        tradeDir.value = ''
        tradeDir.parentElement.classList.toggle('dn')

        window.calculatorReversed = !window.calculatorReversed
    }

    // calculator main function
    async function calculate() {
        // prevent "stale rates" message
        if (window.timeoutCounting) clearInterval(window.timeoutCounting)

        const tradeDir = document.getElementById('tradeDir').value // "buy", "sell", ""
        const crypto = document.getElementById('crypto').value
        const fiat = document.getElementById('fiat').value

        let amount = document.getElementById('amount').value
        amount = validateCurrency(amount)
        amount = parseFloat(amount)
        if (isNaN(amount)) amount = 0

        // if fiat is EUR, convert to NOK because rates are given in NOK/crypto
        const nokPerFiat = await getRate(fiat) // N NOK / 1 fiat  fiat might be EUR

        const nokPerCrypto = await getRate(crypto) // N NOK / 1 crypto

        let valueNOK, value
        if (tradeDir === '') {
            // buy X crypto with N fiat
            valueNOK = amount * nokPerFiat
            value = valueNOK / nokPerCrypto // crypto
        } else {
            valueNOK = amount * nokPerCrypto // fiat
            value = valueNOK / nokPerFiat
        }

        // calculate Fees
        let cryptoFee, fiatFee

        // when 'buy' or 'sell', amount is CRYPTO
        if (tradeDir === 'buy') {
            cryptoFee = calcFee(crypto, amount, "in")
            fiatFee = calcFee(fiat, value, "out")
        } else if (tradeDir === 'sell') {
            cryptoFee = calcFee(crypto, amount, "out")
            fiatFee = calcFee(fiat, value, "in")
        }
        // sum fees, value will be in NOK
        let feeInNOK = cryptoFee * nokPerCrypto / nokPerFiat + fiatFee;
        // change sign if selling crypto
        if (tradeDir === 'sell') feeInNOK *= -1

        // calculating fee in Crypto fiat to crypto
        if (tradeDir === '') {
            cryptoFee = calcFee(crypto, value, 'in')
            fiatFee = calcFee(fiat, amount, 'out')
        }
        let feeInCrypto = cryptoFee + fiatFee * nokPerFiat / nokPerCrypto

        let fiatFormatted = fiat.replace(/-/, ' (') + ')'

        // print output table
        const output = document.getElementById("calculator-output")

        if (window.calculatorReversed) {
            output.innerHTML = `<table class="center pa2 bg-black-80 near-white">
                                    <tr>
                                        <td class="ttc">Sell:</td>
                                        <td class="red">${amount}</td>
                                        <td class="green">${fiatFormatted}</td>
                                    </tr>
                                    <tr>
                                        <td>Base:</td>
                                        <td class="red">${value.toFixed(9)}</td>
                                        <td class="green">${crypto}</td>
                                    </tr>
                                    <tr>
                                        <td>Fee:</td>
                                        <td class="red">${Math.abs(feeInCrypto.toFixed(9))}</td>
                                        <td class="green">${crypto}</td>
                                    </tr>
                                    <tr>
                                        <td>Total:</td>
                                        <td class="red">${(value - feeInCrypto).toFixed(9)}</td>
                                        <td class="green">${crypto}</td>
                                    </tr>
                                </table>`
        } else {
            output.innerHTML = `<table class="center pa2 bg-black-80 near-white">
                                    <tr>
                                        <td class="ttc">${tradeDir}</td>
                                        <td class="red">${amount}</td>
                                        <td class="green">${crypto}</td>
                                    </tr>
                                    <tr>
                                        <td>Base:</td>
                                        <td class="red">${value.toFixed(2)}</td>
                                        <td class="green">${fiatFormatted}</td>
                                    </tr>
                                    <tr>
                                        <td>Fee:</td>
                                        <td class="red">${Math.abs(feeInNOK.toFixed(2))}</td>
                                        <td class="green">${fiatFormatted}</td>
                                    </tr>
                                    <tr>
                                        <td>Total:</td>
                                        <td class="red">${(value + feeInNOK).toFixed(2)}</td>
                                        <td class="green">${fiatFormatted}</td>
                                    </tr>
                                </table>`
        }
        // if wanting to clear the rates, just set a timeout here.
        const TIMEOUT = 60000 // 60 seconds
        window.timeoutCounting = setTimeout(() => {
            output.prepend(document.createTextNode('rates calculated >60 seconds ago'))
            output.firstElementChild.classList.add('ba', 'bw2', 'b--yellow')
        }, TIMEOUT)

        // Calculator Helpers
        // calculate the fee based on currency, amount and trade direction
        // called twice for each trade.
        function calcFee(cur, amount, inout) {
            return json.commissions[cur][inout]['fixed'] + (json.commissions[cur][inout]['ratio'] * amount)
        }

        // Get a rate for a given currency
        async function getRate(cur) {
            // strip all values after -, ie, "NOK-bank" -> "NOK"
            cur = cur.replace(/-(.*)$/, "");
            if (cur === 'NOK') return 1.0

            const url = "http://bitspace.no:5000/api/v1/rates/" + cur;
            const midlabel = cur === 'EUR' ? 'bitgate_cur_mid' : 'bitgate_mid';

            return await fetch(url)
                .then(res => res.json())
                .then(json => {
                    let rates = json.rates[cur + 'NOK']
                    return rates[midlabel][1];
                })
        }

        function validateCurrency(string) {
            const validCurrencyChars = 'eE-0123456789.'
            let output = ''
            for (let char of string) {
                if (validCurrencyChars.indexOf(char) !== 1) output += char
            }
            return output
        }
    }
}())
